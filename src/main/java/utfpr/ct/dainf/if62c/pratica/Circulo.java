package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Omero Francisco Bertol
 */

public class Circulo extends Elipse {
  private double raio;

  public Circulo(double raio) {
    super(raio, raio);
    
    this.raio = raio;
  }

  public void setRaio(double raio) {
    this.raio = raio;
  }

  public double getRaio() {
    return raio;
  }
  
  @Override
  public double getPerimetro() {
    double perimetro;
     
    perimetro = 2 * Math.PI * this.raio;
      
    return(perimetro);
  }
      
}