package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Omero Francisco Bertol
 */

public class Elipse {
  private double eixo1;
  private double eixo2;

  public Elipse(double r, double s) {
    this.eixo1 = r * 2;
    this.eixo2 = s * 2;
  }
    
  public double getEixo1() {
    return eixo1;
  }

  public double getEixo2() {
    return eixo2;
  }
    
  public void setEixo1(double eixo1) {
    this.eixo1 = eixo1;
  }

  public void setEixo2(double eixo2) {
    this.eixo2 = eixo2;
  }
  
  public double getArea() {
    double area;
    double r = this.eixo1 / 2;
    double s = this.eixo2 / 2;
      
    area = Math.PI * r * s;
      
    return(area);
  }
    
  public double getPerimetro() {
    double perimetro;
    double r = this.eixo1 / 2;
    double s = this.eixo2 / 2;
      
    perimetro = Math.PI * ((3 * (r + s)) - Math.sqrt(((3 * r) + s) * (r + (3 * s))));
      
    return(perimetro);
  }
  
}