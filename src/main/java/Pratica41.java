import utfpr.ct.dainf.if62c.pratica.Elipse;
import utfpr.ct.dainf.if62c.pratica.Circulo;

/**
 *
 * @author Omero Francisco Bertol
 */

public class Pratica41 {

  public static void main(String[] args) {
    Elipse elipse = new Elipse(3.0, 6.0);
    Circulo circulo = new Circulo(3.0);
    
    System.out.println("Elipse_______________________");
    System.out.println("Área.....: " + elipse.getArea());
    System.out.println("Perímetro: " + elipse.getPerimetro());
    
    System.out.println();
    
    System.out.println("Círculo______________________");
    System.out.println("Área.....: " + circulo.getArea());
    System.out.println("Perímetro: " + circulo.getPerimetro());     
  }
    
}